const express = require('express');
const bodyParser = require('body-parser');

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'API',
            version: '1.0.0',
            description: 'API Authentication',
        },
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: 'http',
                    scheme: 'bearer',
                    bearerFormat: 'JWT',
                },
            },
        },
        security: [
            {
                bearerAuth: [],
            },
        ],
    },
    apis: ['./routes/*.js'],
    swagger: '/auth/swagger/swagger.json',
};

const swaggerDocument = swaggerJsDoc(swaggerOptions);

const AuthRoutes = require('./routes/AuthRoutes');
const UserRoutes = require('./routes/UserRoutes');
const RoleRoutes = require('./routes/RoleRoutes');
const PermissionRoutes = require('./routes/PermissionRoutes');
const MiddlewareRoutes = require('./routes/MiddlewareRoutes');

// export one function that gets called once as the server is being initialized
module.exports = function (app, server) {
    const mongoose = require('mongoose');
    mongoose.set('strictQuery', false);

    console.log('Starting connexion on host : ', `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_INITDB_PATH}:27017/${process.env.MONGO_INITDB_DATABASE}?authSource=admin`);

    // authSource=admin allows us to connect with admin credentials to any database
    mongoose.connect(`mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@${process.env.MONGO_INITDB_PATH}:27017/${process.env.MONGO_INITDB_DATABASE}?authSource=admin`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
        .then(async () => {
            try {
                console.log("Launching Role seeder");
                const RoleSeeder = require('./seeder/RoleSeeder');
                await RoleSeeder.run();
                console.log("Role seeder completed successfully");
            } catch (error) {
                console.error("Error while running Role seeder:", error);
            }
            
            try {
                console.log("Launching User seeder");
                const UserSeeder = require('./seeder/UserSeeder');
                await UserSeeder.run();
                console.log("User seeder completed successfully");
            } catch (error) {
                console.error("Error while running User seeder:", error);
            }
            
            try {
                console.log("Launching Permission seeder");
                const PermissionSeeder = require('./seeder/PermissionSeeder');
                await PermissionSeeder.run();
                console.log("Permission seeder completed successfully");
            } catch (error) {
                console.error("Error while running Permission seeder:", error);
            }
        })
        .catch((e) => {
            console.log('DB connexion failed', e);
            process.exit(1);
        });

    app.use(express.json());
    app.use(bodyParser.json());

    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', '*');
        res.setHeader('Access-Control-Allow-Methods', '*');
        next();
    });

    //#region ROUTES
    // make route load file at path swagger/swagger.json
    app.get('/auth/swagger/swagger.json', (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(swaggerDocument);
    });

    app.use('/auth/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument, { explorer: true }));
    app.use('/auth/security', AuthRoutes);
    app.use('/auth/users', UserRoutes);
    app.use('/auth/roles', RoleRoutes);
    app.use('/auth/permissions', PermissionRoutes);
    app.use('/auth/middleware', MiddlewareRoutes);
    //#endregion
}
