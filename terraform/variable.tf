variable "environment_suffix" {
  type        = string
  description = "The suffix to append to the environment name"
}

variable "project_name" {
  type    = string
  description = "The name of the project"
}

variable "location" {
  type        = string
  description = "The location of the resource group"
}