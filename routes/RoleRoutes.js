/**
 * @swagger
 * tags:
 *   name: Roles
 *   description: API endpoints for managing roles
 */

const express = require('express');
const router = express.Router();
const RoleController = require('../controllers/RoleController');
const MiddlewareController = require('../controllers/MiddlewareController');

//#region Routes

/**
 * @swagger
 * /auth/roles:
 *   post:
 *     summary: Create Role
 *     description: Create a new role
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/RoleCreateRequest'
 *     responses:
 *       200:
 *         description: Role created successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/RoleResponse'
 */
router.post('/', MiddlewareController.authenticate, RoleController.create);

/**
 * @swagger
 * /auth/roles:
 *   get:
 *     summary: Get Roles
 *     description: Retrieve a list of roles
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Roles retrieved successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/RolesResponse'
 */
router.get('/', MiddlewareController.authenticate, RoleController.get);

/**
 * @swagger
 * /auth/roles/{id}:
 *   get:
 *     summary: Get Role by ID
 *     description: Retrieve a role by ID
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: Role ID
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Role retrieved successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/RoleResponse'
 */
router.get('/:id', MiddlewareController.authenticate, RoleController.getById);

/**
 * @swagger
 * /auth/roles/{id}:
 *   patch:
 *     summary: Update Role
 *     description: Update a role by ID
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: Role ID
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/RoleUpdateRequest'
 *     responses:
 *       200:
 *         description: Role updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/RoleResponse'
 */
router.patch('/:id', MiddlewareController.authenticate, RoleController.update);

/**
 * @swagger
 * /auth/roles/{id}:
 *   delete:
 *     summary: Delete Role
 *     description: Delete a role by ID
 *     tags: [Roles]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: Role ID
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Role deleted successfully
 */
router.delete('/:id', MiddlewareController.authenticate, RoleController.delete);

//#endregion

module.exports = router;
