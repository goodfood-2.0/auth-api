let express = require('express');
let router = express.Router();

const AuthController = require('../controllers/AuthController');
const MiddlewareController = require('../controllers/MiddlewareController');

/**
 * @swagger
 * tags:
 *   name: Authentication
 *   description: API endpoints for authentication
 */

/**
 * @swagger
 * /auth/security/login:
 *   post:
 *     summary: Login
 *     description: Authenticate user and generate access token
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: Successful login
 */
router.post('/login', MiddlewareController.authenticate, AuthController.login);

/**
 * @swagger
 * /auth/security/logout:
 *   delete:
 *     summary: Logout
 *     description: Clear user session and invalidate access token
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: Successful logout
 */
router.delete('/logout', MiddlewareController.authenticate, AuthController.logout);

/**
 * @swagger
 * /auth/security/forgot:
 *   post:
 *     summary: Forgot Password
 *     description: Send password reset email to the user
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: Password reset email sent
 */
router.post('/forgot', MiddlewareController.authenticate, AuthController.forgot);

/**
 * @swagger
 * /auth/security/register:
 *   post:
 *     summary: Register
 *     description: Create a new user account
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: Successful registration
 */
router.post('/register', MiddlewareController.authenticate, AuthController.register);

/**
 * @swagger
 * /auth/security/verify:
 *   post:
 *     summary: Verify Email
 *     description: Verify user's email address
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: Email verification successful
 */
router.post('/verify', MiddlewareController.authenticate, AuthController.verify);

/**
 * @swagger
 * /auth/security/reset:
 *   post:
 *     summary: Reset password
 *     description: Reset user's password
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: Password reset successful
 */
router.post('/reset-password', MiddlewareController.authenticate, AuthController.reset);

/**
 * @swagger
 * /auth/security/token:
 *   post:
 *     summary: Refresh Token
 *     description: Refresh access token using refresh token
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: Token refreshed successfully
 */
router.post('/token', AuthController.token);

/**
 * @swagger
 * /auth/security/me:
 *   get:
 *     summary: Get Current User
 *     description: Get details of the current authenticated user
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: Current user details retrieved successfully
 */
router.get('/me', MiddlewareController.authenticate, AuthController.me);

module.exports = router;
