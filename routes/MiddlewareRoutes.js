/**
 * @swagger
 * tags:
 *   name: Authentication
 *   description: API endpoints for authentication
 */

const express = require('express');
const router = express.Router();
const MiddlewareController = require('../controllers/MiddlewareController');

//#region Routes

/**
 * @swagger
 * /auth/middleware/authenticate:
 *   get:
 *     summary: Authenticate
 *     description: Authenticate user
 *     tags: [Authentication]
 *     responses:
 *       200:
 *         description: User authenticated successfully
 */
router.get('/authenticate', MiddlewareController.middleware);

//#endregion

module.exports = router;
