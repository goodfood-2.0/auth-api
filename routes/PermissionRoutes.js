/**
 * @swagger
 * tags:
 *   name: Permissions
 *   description: API endpoints for managing permissions
 */

const express = require('express');
const router = express.Router();
const PermissionController = require('../controllers/PermissionController');
const MiddlewareController = require('../controllers/MiddlewareController');

//#region Routes

/**
 * @swagger
 * /auth/permissions:
 *   post:
 *     summary: Create Permission
 *     description: Create a new permission
 *     tags: [Permissions]
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/PermissionCreateRequest'
 *     responses:
 *       200:
 *         description: Permission created successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/PermissionResponse'
 */
router.post('/', MiddlewareController.authenticate, PermissionController.create);

/**
 * @swagger
 * /auth/permissions:
 *   get:
 *     summary: Get Permissions
 *     description: Retrieve a list of permissions
 *     tags: [Permissions]
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Permissions retrieved successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/PermissionsResponse'
 */
router.get('/', MiddlewareController.authenticate, PermissionController.get);

/**
 * @swagger
 * /auth/permissions/{id}:
 *   get:
 *     summary: Get Permission by ID
 *     description: Retrieve a permission by ID
 *     tags: [Permissions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: Permission ID
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Permission retrieved successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/PermissionResponse'
 */
router.get('/:id', MiddlewareController.authenticate, PermissionController.getById);

/**
 * @swagger
 * /auth/permissions/{id}:
 *   patch:
 *     summary: Update Permission
 *     description: Update a permission by ID
 *     tags: [Permissions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: Permission ID
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/PermissionUpdateRequest'
 *     responses:
 *       200:
 *         description: Permission updated successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/PermissionResponse'
 */
router.patch('/:id', MiddlewareController.authenticate, PermissionController.update);

/**
 * @swagger
 * /auth/permissions/{id}:
 *   delete:
 *     summary: Delete Permission
 *     description: Delete a permission by ID
 *     tags: [Permissions]
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         description: Permission ID
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Permission deleted successfully
 */
router.delete('/:id', MiddlewareController.authenticate, PermissionController.delete);

//#endregion

module.exports = router;
