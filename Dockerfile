FROM node:16.17.0

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . .

# Generate Swagger documentation
RUN npm run generate-swagger

EXPOSE 3000
CMD [ "npm", "start" ]
