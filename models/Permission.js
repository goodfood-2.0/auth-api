const mongoose = require('mongoose');

const PermissionSchema = mongoose.Schema({
    path: { type: String, required: true },
    regex : { type: String, required: true },
    method: { type: String, required: true },
    _role: { type: mongoose.Schema.Types.ObjectId, ref: 'Role', required: true },
});

module.exports = mongoose.model('Permission', PermissionSchema);