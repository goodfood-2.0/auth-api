const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    email: { type: String, required: true, unique: true, match: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/ },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },

    password: { type: String, required: false },

    verificationToken: { type: String, default: null },
    refreshToken: { type: String, default: null },
    resetPasswordToken: { type: String, default: null },

    has_notifications: { type: Boolean, default: true },

    _role: { type: mongoose.Schema.Types.ObjectId, required: true },
    _restaurant: { type: Number, default: null },

    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
    deleted_at: { type: Date, default: null },
});

module.exports = mongoose.model('User', UserSchema);