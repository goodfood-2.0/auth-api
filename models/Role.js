const mongoose = require('mongoose');

const RoleSchema = mongoose.Schema({
    name: { type: String, required: true, unique: true},
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Role', RoleSchema);