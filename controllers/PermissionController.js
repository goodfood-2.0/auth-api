const Permission = require('../models/Permission');

class PermissionController {

    static create_body = {
        path: {
            type: 'string',
            required: true,
            example: '/permission/create',
        },
    };
    static create = async (req, res) => {
        try {
            let { path } = req.body;

            if (!path) {
                let missing = [];
                if (!path) missing.push('path');

                return res.status(400).json({
                    success: false,
                    message: 'Missing  following informations: ' + missing.join(', '),
                });
            }

            let permission = new Permission({
                path: path,
            });

            permission.save();

            return res.status(200).json({
                success: true,
                message: 'Permission created',
                permission: permission,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'PermissionController.create' });
        }
    };

    static get_query = {
        page: {
            type: 'number',
            required: false,
            example: 1,
        },
        per_page: {
            type: 'number',
            required: false,
            example: 10,
        },
    };
    static get = async (req, res) => {
        try {
            let { page, per_page } = req.query;

            if (!page) page = 1;
            if (!per_page) per_page = 10;

            let permissions = await Permission.find().skip((page - 1) * per_page).limit(per_page).exec();

            return res.status(200).json({
                success: true,
                message: 'Permissions',
                permissions: permissions,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'PermissionController.get' });
        }
    };

    static getById_params = {
        _id: {
            type: 'string',
            required: true,
        },
    };
    static getById = async (req, res) => {
        try {
            let { _id } = req.params;

            if (!_id) {
                let missing = [];
                missing.push('_id');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            let permission = Permission.findById(_id).exec();

            if (!permission) {
                return res.status(404).json({
                    success: false,
                    message: 'Unable to find permission'
                });
            }

            return res.status(200).json({
                success: true,
                message: 'Permission',
                permission: permission,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'PermissionController.getById' });
        }
    };

    static update_params = {
        _id: {
            type: 'string',
            required: true,
        },
    };
    static update_body = {
        path: {
            type: 'string',
            required: true,
            example: '/permission/update',
        },
    };
    static update = async (req, res) => {
        try {
            let { _id } = req.params;
            let { path } = req.body;

            if (!path || !_id) {
                let missing = [];
                if (!path) missing.push('path');
                if (!_id) missing.push('_id');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            let permission = await Permission.findOne({ _id: _id }).exec();

            if (!permission) {
                return res.status(404).json({
                    success: false,
                    message: 'Unable to find permission'
                });
            }

            permission.path = path;
            permission.save();

            return res.status(200).json({
                success: true,
                message: 'Permission updated',
                permission: permission,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'PermissionController.update' });
        }
    };

    static delete_body = {
        _id: {
            type: 'string',
            required: true,
        },
    };
    static delete = async (req, res) => {
        try {
            let { _id } = req.body;

            if (!_id) {
                let missing = [];
                if (!_id) missing.push('_id');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            let permission = Permission.findOne({ _id: _id }).exec();

            if (!permission) {
                return res.status(404).json({
                    success: false,
                    message: 'Unable to find permission'
                });
            }

            permission.delete();

            return res.status(200).json({
                success: true,
                message: 'Permission deleted',
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'PermissionController.delete' });
        }
    };
}

module.exports = PermissionController;