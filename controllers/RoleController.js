const Role = require('../models/Role');

class RoleController {

    static create_body = {
        name: {
            type: 'string',
            required: true,
            example: 'admin',
        },
    };
    static create = async (req, res) => {
        try{
            let { name } = req.body;

            if (!name) {
                let missing = [];
                missing.push('name');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }
    
            let role = new Role({
                name: name,
            });

            role.save();

            return res.status(200).json({
                success: true,
                message: 'Role created',
                role: role,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'RoleController.create' });
        }
    };

    static get_query = {
        page: {
            type: 'number',
            required: false,
            example: 1,
        },
        per_page: {
            type: 'number',
            required: false,
            example: 10,
        },
    };
    static get = async (req, res) => {
        try{
            let { page, per_page } = req.query;

            if (!page) page = 1;
            if (!per_page) per_page = 10;

            let roles = await Role.find().skip((page - 1) * per_page).limit(per_page).exec();

            return res.status(200).json({
                success: true,
                message: 'Roles',
                roles: roles,
            });
        }
        catch(e){
            return res.status(500).json({ error: e, where: 'RoleController.get' });
        }
    };

    static getById_query = {
        _id: {
            type: 'string',
            required: true,
        },
    };
    static getById = async (req, res) => {
        try{
            let { _id } = req.query;

            if (!_id) {
                let missing = [];
                missing.push('_id');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }
            
            let role = await Role.findById(_id).exec();

            if (!role) {
                return res.status(404).json({
                    success: false,
                    message: 'Unable to find role'
                });
            }

            return res.status(200).json({
                success: true,
                message: 'Role',
                role: role,
            });
        }
        catch(e){
            return res.status(500).json({ error: e, where: 'RoleController.getById' });
        }
    };

    static update_params = {
        _id: {
            type: 'string',
            required: true,
        },
    };
    static update_body = {
        name: {
            type: 'string',
            required: true,
            example: 'admin',
        },
    };
    static update = async (req, res) => {
        try{
            let { _id, name } = req.body;

            if (!name || !_id) {
                let missing = [];
                if (!name) missing.push('name');
                if (!_id) missing.push('_id');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            let role = await Role.findById(_id).exec();
            if (!role) {
                return res.status(404).json({
                    success: false,
                    message: 'Unable to find role'
                });
            }

            role.name = name;
            role.save();

            return res.status(200).json({
                success: true,
                message: 'Role updated',
                role: role,
            });
        }
        catch(e){
            return res.status(500).json({ error: e, where: 'RoleController.update' });
        }
    };

    static delete_params = {
        _id: {
            type: 'string',
            required: true,
        },
    };
    static delete = async (req, res) => {
        try{

            let { _id } = req.params._id;

            if (!_id) {
                let missing = [];
                missing.push('_id');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            let role = await Role.findById(_id).exec();

            if (!role) {
                return res.status(404).json({
                    success: false,
                    message: 'Role not found'
                });
            }

            role.remove();

            return res.status(200).json({
                success: true,
                message: 'Role deleted',
            });
        }
        catch(e){
            return res.status(500).json({ error: e, where: 'RoleController.delete' });
        }
    };
}

module.exports = RoleController;