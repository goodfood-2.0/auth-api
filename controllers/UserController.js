const User = require('../models/User');
const amqp = require('amqplib');

const { randomKey } = require('../utils/random');

class UserController {
    static create = async (req, res) => {
        try {
            let { email, firstname, lastname, _role, _restaurant } = req.body;

            if (!email || !firstname || !lastname || !_role || !_restaurant) {
                return res.status(400).json({
                    success: false,
                    message: 'Missing informations'
                });
            }

            let verificationToken = randomKey(32);

            let user = new User({
                email: email,
                firstname: firstname,
                lastname: lastname,
                verificationToken: verificationToken,
                _role: _role,
                _restaurant: _restaurant,
            });

            user.save();

            const msg = {
                command: "sendWelcomeEmail",
                myObject : {
                    email: email,
                    token: user.verificationToken,
                }
            };

            const connection = await amqp.connect("amqp://" +process.env.RABBITMQ_URL + ":5672");
            const channel = await connection.createChannel();
            await channel.assertQueue('mailing', { durable: true });
            await channel.sendToQueue('mailing', Buffer.from(JSON.stringify(msg)), { persistent: true });

            return res.status(200).json({
                success: true,
                message: 'User created',
                user: user,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'UserController.create' });
        }
    };

    static get = async (req, res) => {
        try {
            let users = await User.find({}).exec();

            return res.status(200).json({
                success: true,
                message: 'Users',
                users: users,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'UserController.get' });
        }
    };

    static getById = async (req, res) => {
        try {
            let user = await User.findById(req.params.id).exec();

            if (!user) {
                return res.status(404).json({
                    success: false,
                    message: 'User not found',
                });
            }

            return res.status(200).json({
                success: true,
                message: 'User',
                user: user,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'UserController.getById' });
        }
    };

    static update = async (req, res) => {
        try {
            let { _id, email, firstname, lastname, _role, _restaurant } = req.body;

            // if there is an id but no field
            if (!_id) {
                return res.status(400).json({
                    success: false,
                    message: 'No id provided'
                });
            }

            if (!email && !firstname && !lastname && !_role && !_restaurant) {
                return res.status(400).json({
                    success: false,
                    message: 'Missing informations'
                });
            }

            let user = await User.find({ _id: _id }).exec();

            if (!user) {
                return res.status(400).json({
                    success: false,
                    message: 'User not found'
                });
            }

            user.email = email ?? user.email;
            user.firstname = firstname ?? user.firstname;
            user.lastname = lastname ?? user.lastname;
            user._role = _role ?? user._role;
            user._restaurant = _restaurant ?? user._restaurant;

            user.save();

            return res.status(200).json({
                success: true,
                message: 'User updated',
                user: user,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'UserController.update' });
        }
    };

    static delete = async (req, res) => {
        try {
            let { _id } = req.body;

            if (!_id) {
                return res.status(400).json({
                    success: false,
                    message: 'No id provided'
                });
            }

            let user = await User.findOne({ _id: _id }).exec();

            if (!user) {
                return res.status(400).json({
                    success: false,
                    message: 'User not found'
                });
            }

            user.deleted_at = new Date();
            user.save();

            return res.status(200).json({
                success: true,
                message: 'User deleted',
                user: user,
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'UserController.delete' });
        }
    };
}

module.exports = UserController;