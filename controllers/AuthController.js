const jwt = require('jsonwebtoken');
const amqp = require('amqplib');

const User = require('../models/User');
const Role = require('../models/Role');

const { cryptPassword, comparePassword } = require('../utils/password');
const { randomKey } = require('../utils/random');

class AuthController {

    static login = async (req, res) => {
        try {
            const { email, password } = req.body;

            if (!email || !password) {
                let missing = [];
                if (!email) missing.push('email');
                if (!password) missing.push('password');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            let user = await User.findOne({ email: email, verificationToken: null, deleted_at: null }).exec();
            if (!user) {
                return res.status(401).json({
                    success: false,
                    message: 'Unable to find user'
                });
            }

            let { err, match } = await comparePassword(password, user.password);
            if (err) {
                console.log(err);
                return res.status(500).json({ error: err });
            }

            if (!match) {
                return res.status(401).json({
                    success: false,
                    message: 'Wrong credentials'
                });
            }

            let role = await Role.findOne({ _id: user._role}).exec();

            const accessToken = generateAccessToken(user);
            const refreshToken = generateRefreshToken(user);

            user.refreshToken = refreshToken;
            user.save();

            return res.status(200).json({
                success: true,
                message: 'Authentication successful',
                accessToken: accessToken,
                refreshToken: refreshToken,
                user: {
                    _id: user._id,
                    email: user.email,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    has_notification: user.has_notification,
                    created_at: user.created_at,
                    updated_at: user.updated_at,
                    _role : role,
                    _restaurant: user._restaurant,
                }
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'AuthController.login' });
        }
    };

    static register = async (req, res) => {
        try {
            const { email, firstname, lastname } = req.body;

            if (!email || !firstname || !lastname) {
                let missing = [];
                if (!email) missing.push('email');
                if (!firstname) missing.push('firstname');
                if (!lastname) missing.push('lastname');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            let mail_regex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
            if (!mail_regex.test(email)) {
                return res.status(400).json({
                    success: false,
                    message: 'Email is not valid'
                });
            }
            
            let exists = await User.findOne({ email: email }).exec();
            if (exists) {
                return res.status(409).json({
                    success: false,
                    message: 'Email already used'
                });
            }

            let role = await Role.findOne({ name: 'client' }).exec();
            const user = new User({
                email: email,
                firstname: firstname,
                lastname: lastname,

                verificationToken: randomKey(32),

                _role: role._id,
            });
            
            user.save();

            const msg = {
                command: "sendWelcomeEmail",
                myObject :{
                    email: email,
                    token: user.verificationToken,
                }
            };

            try {
                const connection = await amqp.connect("amqp://" +process.env.RABBITMQ_URL + ":5672");
                const channel = await connection.createChannel();
                await channel.assertQueue('mailing', { durable: true });
                await channel.sendToQueue('mailing', Buffer.from(JSON.stringify(msg)), { persistent: true });
            }
            catch(e){
                console.log("Erreur lors de l'envois du mail de créaion de compte : ", e);

                return res.status(202).json({
                    success: true,
                    message: 'User registered successfully, token is in response',
                    token: user.verificationToken
                });
            }

            return res.status(201).json({
                success: true,
                message: 'User registered successfully'
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'AuthController.register' });
        }
    };

    static token = async (req, res) => {
        try {
            const refreshToken = req.body.refreshToken;
            if (refreshToken == null) return res.sendStatus(401);
            // if resfresh token does not exist in db, return 403
            let exists = await User.findOne({ refreshToken: refreshToken, verificationToken: null, deleted_at: null }).exec();
            if (!exists) return res.sendStatus(403);

            jwt.verify(refreshToken, process.env.JWT_REFRESH_KEY, (err, user) => {
                if (err) return res.sendStatus(403);
                const accessToken = generateAccessToken(user);
                res.json({ accessToken: accessToken });
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'AuthController.token' });
        }
    };

    static logout = async (req, res) => {
        try {
            let exists = await User.findOne({ _id: req.user._id }).exec();
            if (!exists) return res.sendStatus(403);

            exists.refreshToken = null;
            exists.save();

            res.status(200).json({ success: true, message: 'Logout successful' });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'AuthController.logout' });
        }
    };

    static verify = async (req, res) => {
        try {
            const { password, password_confirm } = req.body;
            const { verificationToken } = req.query;

            if (!verificationToken || !password || !password_confirm) {
                let missing = [];
                if (!verificationToken) missing.push('verificationToken');
                if (!password) missing.push('password');
                if (!password_confirm) missing.push('password_confirm');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            if (password !== password_confirm) {
                return res.status(400).json({
                    success: false,
                    message: 'Passwords does not match'
                });
            }

            let exists = await User.findOne({ verificationToken: verificationToken, deleted_at: null }).exec();
            if (!exists) {
                return res.status(401).json({
                    success: false,
                    message: 'Unable to find user'
                });
            }

            let password_regex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
            if (!password_regex.test(password)) {
                return res.status(400).json({
                    success: false,
                    message: 'Password does not match requirement'
                });
            }

            let { err, hash } = await cryptPassword(password);
            if (err) {
                return res.status(500).json({ error: err });
            }

            exists.password = hash;
            exists.verificationToken = null;
            exists.save();

            return res.status(200).json({
                success: true,
                message: 'Verification successful'
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'AuthController.verify' });
        }
    };

    static reset = async (req, res) => {
        try {
            const { token, password, password_confirm } = req.body;

            if (!token || !password || !password_confirm) {
                let missing = [];
                if (!token) missing.push('verificationToken');
                if (!password) missing.push('password');
                if (!password_confirm) missing.push('password_confirm');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            if (password !== password_confirm) {
                return res.status(400).json({
                    success: false,
                    message: 'Passwords does not match'
                });
            }

            let exists = await User.findOne({ resetPasswordToken: token, deleted_at: null }).exec();
            if (!exists) {
                return res.status(401).json({
                    success: false,
                    message: 'Unable to find user'
                });
            }

            let password_regex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
            if (!password_regex.test(password)) {
                return res.status(400).json({
                    success: false,
                    message: 'Password does not match requirement'
                });
            }

            let { err, hash } = await cryptPassword(password);
            if (err) {
                return res.status(500).json({ error: err });
            }

            exists.password = hash;
            exists.resetPasswordToken = null;
            exists.save();

            return res.status(200).json({
                success: true,
                message: 'Password reset successful'
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'AuthController.reset' });
        }
    };

    static forgot = async (req, res) => {
        try {
            const { email } = req.body;

            if (!email) {
                let missing = [];
                if (!email) missing.push('email');

                return res.status(400).json({
                    success: false,
                    message: 'Missing following informations: ' + missing.join(', '),
                });
            }

            let exist = await User.findOne({ email: email, verificationToken: null, deleted_at: null }).exec();
            if (!exist) {
                return res.status(401).json({
                    success: false,
                    message: 'Unable to find user'
                });
            }

            exist.resetPasswordToken = randomKey(32);
            exist.save();

            const msg = {
                command: "sendForgotPasswordEmail",
                myObject :{
                    email: exist.email,
                    token: exist.resetPasswordToken,
                }
            };

            try {
                const connection = await amqp.connect("amqp://" +process.env.RABBITMQ_URL + ":5672");
                const channel = await connection.createChannel();
                await channel.assertQueue('mailing', { durable: true });
                await channel.sendToQueue('mailing', Buffer.from(JSON.stringify(msg)), { persistent: true });
            }
            catch(e){
                console.log("Erreur lors de l'envois du mail de réinitialisation de compte : ", e);

                return res.status(202).json({
                    success: true,
                    message: 'Reset password token generated, mail sent.',
                });
            }

            return res.status(200).json({
                success: true,
                message: 'Reset password token generated'
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'AuthController.forgot' });
        }
    };

    static me = async (req, res) => {
        try {
            var exist = await User.findOne({ _id: req.user._id }).exec();
            if (!exist) {
                return res.status(401).json({
                    success: false,
                    message: 'Unable to find user'
                });
            }

            let role = await Role.findOne({ _id: exist._role}).exec();

            return res.status(200).json({
                success: true,
                message: 'Session valid',
                user: {
                    _id: exist._id,
                    email: exist.email,
                    firstname: exist.firstname,
                    lastname: exist.lastname,
                    has_notification: exist.has_notification,
                    created_at: exist.created_at,
                    updated_at: exist.updated_at,
                    role : role,
                    _restaurant: exist._restaurant
                }
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'AuthController.me' });
        }
    };
}

const generateAccessToken = (user) => {
    return jwt.sign({
        _id: user._id,
        email: user.email,
    },
        process.env.JWT_ACCESS_KEY,
        { expiresIn: '30min' });
}

const generateRefreshToken = (user) => {
    return jwt.sign({
        _id: user._id,
        email: user.email,
    },
        process.env.JWT_ACCESS_KEY,
        { expiresIn: '7d' });
}

module.exports = AuthController;
