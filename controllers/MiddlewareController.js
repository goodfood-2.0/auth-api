const jwt = require('jsonwebtoken');

const User = require('../models/User');
const Permission = require('../models/Permission');

class MiddlewareController {

    static middleware = async (req, res, next) => {
        try {
            let xAuthRequestRedirect = req.headers['x-auth-request-redirect'];
            let method = req.headers['x-original-method'].toUpperCase();;
            let path = xAuthRequestRedirect.split('?')[0];
            if (path[path.length - 1] === '/') {
                path = path.slice(0, -1);
            }

            console.log('User tried to connect to services with this validation : ');
            console.log('path', path);
            console.log('method', method);

            // ON A FAIT DES COMPROMIS, EXCUSEZ NOUS
            let path_prefix = "/" + path.split('/')[1] + "/" + path.split('/')[2]; 
            const regexPattern = new RegExp(path_prefix, "i");

            let permissions = await Permission.find({ path: regexPattern, method: method }).exec();
            let permission = null;

            for (let i = 0; i < permissions.length; i++) {
                if (new RegExp(permissions[i].regex).test(path)) {
                    permission = permissions[i];
                    break;
                }
            }
            
            console.log('Permission list : ', permissions);
            console.log('Permission found : ', permission);

            if (!permission) {
                console.log('no permission, so next');
                return res.status(200).json({
                    success: true,
                    message: 'No permission needed'
                });
            }

            const authHeader = req.headers['authorization'];
            const token = authHeader && authHeader.split(' ')[1];

            console.log('Auth header : ', authHeader);
            console.log('Header Token : ', token);

            if (token == null) {
                return res.status(401).json({
                    success: false,
                    message: 'Error, token not found'
                });
            }

            jwt.verify(token, process.env.JWT_ACCESS_KEY, async (err, p_user) => {
                if (err) {
                    return res.status(403).json({
                        success: false,
                        message: 'Error verification JWT'
                    });
                }

                let user = await User.findOne({ _id: p_user._id, verificationToken: null, deleted_at: null }).exec();
                if (!user) {
                    return res.status(401).json({
                        success: false,
                        message: 'Unauthorized route'
                    });
                }

                // let autorized = await RolePermission.findOne({ _role: user._role, _permission: permission._id }).exec();
                console.log('user', user);
                console.log('permission', permission);
                if (!user._role == permission._role) {
                    return res.status(401).json({
                        success: false,
                        message: 'Unauthorized route'
                    });
                }

                res.setHeader('UserID', user._id); // Set the UserID header
                return res.status(200).json({
                    success: true,
                    message: 'User authorized'
                });
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'MiddlewareController.authenticate' });
        }
    }

    static authenticate = async (req, res, next) => {
        try {
            let method = req.method.toUpperCase();
            let path = req.originalUrl.split('?')[0];
            if (path[path.length - 1] === '/') {
                path = path.slice(0, -1);
            }

            // ON A FAIT DES COMPROMIS, EXCUSEZ NOUS
            let path_prefix = "/" + path.split('/')[1] + "/" + path.split('/')[2]; 
            const regexPattern = new RegExp(path_prefix, "i");

            let permissions = await Permission.find({ path: regexPattern, method: method }).exec();
            let permission = null;

            for (let i = 0; i < permissions.length; i++) {
                if (new RegExp(permissions[i].regex).test(path)) {
                    permission = permissions[i];
                    break;
                }
            }
            
            console.log('permission', permission);

            if (!permission) {
                next();
                return;
            }

            console.log('User tried to connect to services with this validation : ');
            console.log('path', path);
            console.log('method', method);
            console.log('permission', permission);

            const authHeader = req.headers['authorization'];
            const token = authHeader && authHeader.split(' ')[1];
            if (token == null) {
                return res.sendStatus(401);
            }

            jwt.verify(token, process.env.JWT_ACCESS_KEY, async (err, p_user) => {
                if (err) {
                    return res.status(403).json({
                        success: false,
                        message: 'Error verification JWT'
                    });
                }

                let user = await User.findOne({ _id: p_user._id, verificationToken: null, deleted_at: null }).exec();
                if (!user) {
                    return res.status(401).json({
                        success: false,
                        message: 'Unauthorized route'
                    });
                }

                if (!user._role == permission._role) {
                    return res.status(401).json({
                        success: false,
                        message: 'Unauthorized route'
                    });
                }

                req.user = {
                    _id: user._id,
                    email: user.email,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    has_notification: user.has_notification,
                    created_at: user.created_at,
                    updated_at: user.updated_at,
                    _role : user._role,
                    _restaurant: user._restaurant,
                };

                next();
                return;
            });
        }
        catch (e) {
            return res.status(500).json({ error: e, where: 'MiddlewareController.authenticate' });
        }
    }
}

module.exports = MiddlewareController;