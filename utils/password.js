let bcrypt = require('bcrypt');

function cryptPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, (err, salt) => {
            if (err) {
                reject(err);
            }
            bcrypt.hash(password, salt, (err, hash) => {
                if (err) {
                    reject(err);
                }
                resolve({ err, hash });
            });
        });
    });
}

function comparePassword(plainPass, hashword) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(plainPass, hashword, (err, match) => {
            if (err) {
                reject(err);
            }
            resolve({ err, match });
        });
    });
}

module.exports = {
    cryptPassword,
    comparePassword
}