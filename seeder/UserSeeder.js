const { cryptPassword } = require('../utils/password');
const User = require('../models/User');
const Role = require('../models/Role');

class UserSeeder {
    static async run() {
        try {
            let exists = await User.exists({ email: process.env.ADMIN_EMAIL }).exec();
            if (exists) {
                return;
            }

            let { err, hash } = await cryptPassword(process.env.ADMIN_PASS);
            if (err) {
                throw new Error(err);
            }

            let role = await Role.findOne({ name: "admin" }).exec();
            
            const admin = new User({
                email: process.env.ADMIN_EMAIL,
                firstname: "Admin",
                lastname: "Admin",
                password: hash,
                _role: role._id,
            });

            admin.save();

            console.log("Admin user created");

            ///////////////////////////

            // take before and after @
            let base = process.env.ADMIN_EMAIL.split("@")[0];
            let domain = process.env.ADMIN_EMAIL.split("@")[1];

            exists = await User.exists({ email: base + "+1@" + domain }).exec();
            if (exists) {
                return;
            }

            role = await Role.findOne({ name: "accountant" }).exec();

            const accountant = new User({
                email: base + "+1@" + domain,
                firstname: "Admin",
                lastname: "Admin",
                password: hash,
                _role: role._id,
            });
            
            accountant.save();

            console.log("Accountant user created");

            ///////////////////////////

            exists = await User.exists({ email: base + "+2@" + domain }).exec();
            if (exists) {
                return;
            }

            role = await Role.findOne({ name: "deliverer" }).exec();

            const deliverer = new User({
                email: base + "+2@" + domain,
                firstname: "Admin",
                lastname: "Admin",
                password: hash,
                _role: role._id,
            });

            deliverer.save();

            console.log("Deliverer user created");

            ///////////////////////////

            exists = await User.exists({ email: base + "+3@" + domain }).exec();
            if (exists) {
                return;
            }

            role = await Role.findOne({ name: "client" }).exec();

            const client = new User({
                email: base + "+3@" + domain,
                firstname: "Admin",
                lastname: "Admin",
                password: hash,
                _role: role._id,
            });

            client.save();

            console.log("Client user created");
        }
        catch (e) {
            console.log(e);
        }
    }
}

module.exports = UserSeeder;