const Role = require('../models/Role');
const Permission = require('../models/Permission');

class PermissionSeeder {
    static permissionsByRole = {
        admin: [
            //#region auth-api
            { path: '/auth/users', regex: "^\\/auth\\/users$", method: 'POST' },
            { path: '/auth/users', regex: "^\\/auth\\/users$", method: 'GET' },
            { path: '/auth/users/{id}', regex: "\\/auth\\/users\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/auth/users/{id}', regex: "\\/auth\\/users\\/[a-zA-Z0-9]+", method: 'PATCH' },
            { path: '/auth/users/{id}', regex: "\\/auth\\/users\\/[a-zA-Z0-9]+", method: 'DELETE' },
            
            { path: '/auth/roles', regex: "^\\/auth\\/roles$", method: 'POST' },
            { path: '/auth/roles', regex: "^\\/auth\\/roles$", method: 'GET' },
            { path: '/auth/roles/{id}', regex: "\\/auth\\/roles\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/auth/roles/{id}', regex: "\\/auth\\/roles\\/[a-zA-Z0-9]+", method: 'PATCH' },
            { path: '/auth/roles/{id}', regex: "\\/auth\\/roles\\/[a-zA-Z0-9]+", method: 'DELETE' },
            
            { path: '/auth/permissions', regex: "^\\/auth\\/permissions$", method: 'POST' },
            { path: '/auth/permissions', regex: "^\\/auth\\/permissions$", method: 'GET' },
            { path: '/auth/permissions/{id}', regex: "\\/auth\\/permissions\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/auth/permissions/{id}', regex: "\\/auth\\/permissions\\/[a-zA-Z0-9]+", method: 'PATCH' },
            { path: '/auth/permissions/{id}', regex: "\\/auth\\/permissions\\/[a-zA-Z0-9]+", method: 'DELETE' },
    
            { path: '/auth/security/logout', regex: "^\\/auth\\/security\\/logout$", method: 'DELETE' },
            { path: '/auth/security/token', regex: "^\\/auth\\/security\\/token$", method: 'POST' },
            { path: '/auth/security/me', regex: "^\\/auth\\/security\\/me$", method: 'GET' },
            //#endregion auth-api
            //#region restaurant-api
            { path: '/restaurant/employees', regex: "^\\/restaurant\\/employees$", method: 'GET' },
            { path: '/restaurant/employees', regex: "^\\/restaurant\\/employees$", method: 'POST' },
            { path: '/restaurant/employees/{id}', regex: "^\\/restaurant\\/employees\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/employees/{id}', regex: "^\\/restaurant\\/employees\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/employees/{id}', regex: "^\\/restaurant\\/employees\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/restaurant/ingredient-suppliers', regex: "^\\/restaurant\\/ingredient-suppliers$", method: 'GET' },
            { path: '/restaurant/ingredient-suppliers/{id_ingredient}/{id_supplier}', regex: "^\\/restaurant\\/ingredient-suppliers\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/ingredient-suppliers/{id_ingredient}/{id_supplier}', regex: "^\\/restaurant\\/ingredient-suppliers\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/ingredient-suppliers/{id_ingredient}/{id_supplier}', regex: "^\\/restaurant\\/ingredient-suppliers\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'DELETE' },
            
            { path: '/restaurant/restaurant-suppliers', regex: "^\\/restaurant\\/restaurant-suppliers$", method: 'GET' },
            { path: '/restaurant/restaurant-suppliers', regex: "^\\/restaurant\\/restaurant-suppliers\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/{id_restaurant}/suppliers/{id_supplier}', regex: "^\\/restaurant\\/[a-zA-Z0-9]+\\/-suppliers\\/[a-zA-Z0-9]+", method: 'DELETE' },
            
            { path: '/restaurant/restaurants', regex: "^\\/restaurant\\/restaurants$", method: 'POST' },
            { path: '/restaurant/restaurants/{id}', regex: "^\\/restaurant\\/restaurants\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/restaurants/{id}', regex: "^\\/restaurant\\/restaurants\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/restaurant/stocks', regex: "^\\/restaurant\\/stocks$", method: 'GET' },
            { path: '/restaurant/stocks/{id_restaurant}/{id_ingredient}', regex: "^\\/restaurant\\/stocks\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/stocks/{id_restaurant}/{id_ingredient}', regex: "^\\/restaurant\\/stocks\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/stocks/{id_restaurant}/{id_ingredient}', regex: "^\\/restaurant\\/stocks\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'DELETE' },
            
            { path: '/restaurant/suppliers', regex: "^\\/restaurant\\/suppliers$", method: 'GET' },
            { path: '/restaurant/suppliers', regex: "^\\/restaurant\\/suppliers$", method: 'POST' },
            { path: '/restaurant/suppliers/{id}', regex: "^\\/restaurant\\/suppliers\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/suppliers/{id}', regex: "^\\/restaurant\\/suppliers\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/suppliers/{id}', regex: "^\\/restaurant\\/suppliers\\/[a-zA-Z0-9]+", method: 'DELETE' },
            //#endregion restaurant-api
            //#region catalog-api
            { path: '/catalog/ingredients', regex: "^\\/catalog\\/ingredients$", method: 'POST' },
            { path: '/catalog/ingredients/{id}', regex: "^\\/catalog\\/ingredients\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/catalog/ingredients/{id}', regex: "^\\/catalog\\/ingredients\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/catalog/product-ingredients/{product}/ingredient/{ingredient}', regex: "^\\/catalog\\/product-ingredients\\/[a-zA-Z0-9]+\\/ingredient\\/[a-zA-Z0-9]+", method: 'POST' },
            { path: '/catalog/product-ingredients/{product}/ingredient/{ingredient}', regex: "^\\/catalog\\/product-ingredients\\/[a-zA-Z0-9]+\\/ingredient\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/catalog/products', regex: "^\\/catalog\\/products$", method: 'POST' },
            { path: '/catalog/products/{id}', regex: "^\\/catalog\\/products\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/catalog/products/{id}', regex: "^\\/catalog\\/products\\/[a-zA-Z0-9]+", method: 'DELETE' },
            //#endregion catalog-api
            //#region reporting-api
            { path: '/reporting/ReportingOrder/GetOrderBestSold', regex: "\\/reporting\\/ReportingOrder\\/GetOrderBestSold$", method: 'GET' },
            { path: '/reporting/ReportingOrder/GetOrderBestContents', regex: "\\/reporting\\/ReportingOrder\\/GetOrderBestContents$", method: 'GET' },
            { path: '/reporting/ReportingOrder/CountOrders', regex: "\\/reporting\\/ReportingOrder\\/CountOrders$", method: 'GET' },
            { path: '/reporting/ReportingOrder/GetTotalAmount', regex: "\\/reporting\\/ReportingOrder\\/GetTotalAmount$", method: 'GET' },
            //#endregion reporting-api
            //#region order-api
            { path: '/order/Baskets', regex: "\\/order\\/Baskets$", method: 'GET' },

            { path: '/order/OrderContents', regex: "\\/order\\/OrderContents$", method: 'GET' },
            { path: '/order/OrderContents/{id}', regex: "\\/order\\/OrderContents\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/order/OrderContents/GetOrderBestContents', regex: "\\/order\\/OrderContents\\/GetOrderBestContents$", method: 'GET' },
            { path: '/order/OrderContents/GetOrderBestSold', regex: "\\/order\\/OrderContents\\/GetOrderBestSold$", method: 'GET' },
            { path: '/order/OrderContents/GetTotalAmount', regex: "\\/order\\/OrderContents\\/GetTotalAmount$", method: 'GET' },
            { path: '/order/Orders/CountOrders', regex: "\\/order\\/Orders\\/CountOrders", method: 'GET' },

            { path: '/order/Orders', regex: "\\/order\\/Orders$", method: 'GET' },
            { path: '/order/Orders/{id}', regex: "\\/order\\/Orders\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/order/Orders/{id}', regex: "\\/order\\/Orders\\/[a-zA-Z0-9]+", method: 'DELETE' },
            //#endregion order-api
            //#region delivery-api
            { path: '/delivery/Deliveries', regex: "\\/delivery\\/Deliveries$", method: 'GET' },
            { path: '/delivery/Deliveries/{id}', regex: "\\/delivery\\/Deliveries\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/delivery/Deliveries/{id}', regex: "\\/delivery\\/Deliveries\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/delivery/Deliveries/Restaurant/{id}', regex: "\\/delivery\\/Deliveries\\/Restaurant\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/delivery/Deliveries/ChangeStatus', regex: "\\/delivery\\/Deliveries\\/ChangeStatus$", method: 'PUT' },

            { path: '/delivery/StatusesDeliveries', regex: "\\/delivery\\/StatusesDeliveries$", method: 'GET' },
            { path: '/delivery/StatusesDeliveries', regex: "\\/delivery\\/StatusesDeliveries$", method: 'POST' },
            { path: '/delivery/StatusesDeliveries/{id}', regex: "\\/delivery\\/StatusesDeliveries\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/delivery/StatusesDeliveries/{id}', regex: "\\/delivery\\/StatusesDeliveries\\/[a-zA-Z0-9]+", method: 'DELETE' }
            //#endregion delivery-api
        ],
        accountant: [
            //#region auth-api
            { path: '/auth/security/logout', regex: "^\\/auth\\/security\\/logout$", method: 'DELETE' },
            { path: '/auth/security/token', regex: "^\\/auth\\/security\\/token$", method: 'POST' },
            { path: '/auth/security/me', regex: "^\\/auth\\/security\\/me$", method: 'GET' },
            //#endregion auth-api
            //#region restaurant-api
            { path: '/restaurant/employees', regex: "^\\/restaurant\\/employees$", method: 'GET' },
            { path: '/restaurant/employees', regex: "^\\/restaurant\\/employees$", method: 'POST' },
            { path: '/restaurant/employees/{id}', regex: "^\\/restaurant\\/employees\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/employees/{id}', regex: "^\\/restaurant\\/employees\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/employees/{id}', regex: "^\\/restaurant\\/employees\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/restaurant/ingredient-suppliers', regex: "^\\/restaurant\\/ingredient-suppliers$", method: 'GET' },
            { path: '/restaurant/ingredient-suppliers/{id_ingredient}/{id_supplier}', regex: "^\\/restaurant\\/ingredient-suppliers\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/ingredient-suppliers/{id_ingredient}/{id_supplier}', regex: "^\\/restaurant\\/ingredient-suppliers\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/ingredient-suppliers/{id_ingredient}/{id_supplier}', regex: "^\\/restaurant\\/ingredient-suppliers\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'DELETE' },
            
            { path: '/restaurant/restaurant-suppliers', regex: "^\\/restaurant\\/restaurant-suppliers$", method: 'GET' },
            { path: '/restaurant/restaurant-suppliers', regex: "^\\/restaurant\\/restaurant-suppliers\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/{id_restaurant}/suppliers/{id_supplier}', regex: "^\\/restaurant\\/[a-zA-Z0-9]+\\/-suppliers\\/[a-zA-Z0-9]+", method: 'DELETE' },
            
            { path: '/restaurant/restaurant/{id}', regex: "^\\/restaurant\\/restaurant\\/[a-zA-Z0-9]+", method: 'PUT' },

            { path: '/restaurant/stocks', regex: "^\\/restaurant\\/stocks$", method: 'GET' },
            { path: '/restaurant/stocks/{id_restaurant}/{id_ingredient}', regex: "^\\/restaurant\\/stocks\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/stocks/{id_restaurant}/{id_ingredient}', regex: "^\\/restaurant\\/stocks\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/stocks/{id_restaurant}/{id_ingredient}', regex: "^\\/restaurant\\/stocks\\/[a-zA-Z0-9]+\\/[a-zA-Z0-9]+", method: 'DELETE' },
            
            { path: '/restaurant/suppliers', regex: "^\\/restaurant\\/suppliers$", method: 'GET' },
            { path: '/restaurant/suppliers', regex: "^\\/restaurant\\/suppliers$", method: 'POST' },
            { path: '/restaurant/suppliers/{id}', regex: "^\\/restaurant\\/suppliers\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/restaurant/suppliers/{id}', regex: "^\\/restaurant\\/suppliers\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/restaurant/suppliers/{id}', regex: "^\\/restaurant\\/suppliers\\/[a-zA-Z0-9]+", method: 'DELETE' },
            //#endregion restaurant-api
            //#region catalog-api
            { path: '/catalog/ingredients', regex: "^\\/catalog\\/ingredients$", method: 'POST' },
            { path: '/catalog/ingredients/{id}', regex: "^\\/catalog\\/ingredients\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/catalog/ingredients/{id}', regex: "^\\/catalog\\/ingredients\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/catalog/product-ingredients/{product}/ingredient/{ingredient}', regex: "^\\/catalog\\/product-ingredients\\/[a-zA-Z0-9]+\\/ingredient\\/[a-zA-Z0-9]+", method: 'POST' },
            { path: '/catalog/product-ingredients/{product}/ingredient/{ingredient}', regex: "^\\/catalog\\/product-ingredients\\/[a-zA-Z0-9]+\\/ingredient\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/catalog/products', regex: "^\\/catalog\\/products$", method: 'POST' },
            { path: '/catalog/products/{id}', regex: "^\\/catalog\\/products\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/catalog/products/{id}', regex: "^\\/catalog\\/products\\/[a-zA-Z0-9]+", method: 'DELETE' },
            //#endregion catalog-api
            //#region reporting-api
            { path: '/reporting/ReportingOrder/GetOrderBestSold', regex: "\\/reporting\\/ReportingOrder\\/GetOrderBestSold$", method: 'GET' },
            { path: '/reporting/ReportingOrder/GetOrderBestContents', regex: "\\/reporting\\/ReportingOrder\\/GetOrderBestContents$", method: 'GET' },
            { path: '/reporting/ReportingOrder/CountOrders', regex: "\\/reporting\\/ReportingOrder\\/CountOrders$", method: 'GET' },
            { path: '/reporting/ReportingOrder/GetTotalAmount', regex: "\\/reporting\\/ReportingOrder\\/GetTotalAmount$", method: 'GET' },
            //#endregion reporting-api
            //#region order-api
            { path: '/order/Baskets', regex: "\\/order\\/Baskets$", method: 'GET' },

            { path: '/order/OrderContents', regex: "\\/order\\/OrderContents$", method: 'GET' },
            { path: '/order/OrderContents/{id}', regex: "\\/order\\/OrderContents\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/order/OrderContents/GetOrderBestContents', regex: "\\/order\\/OrderContents\\/GetOrderBestContents$", method: 'GET' },
            { path: '/order/OrderContents/GetOrderBestSold', regex: "\\/order\\/OrderContents\\/GetOrderBestSold$", method: 'GET' },
            { path: '/order/OrderContents/GetTotalAmount', regex: "\\/order\\/OrderContents\\/GetTotalAmount$", method: 'GET' },
            { path: '/order/Orders/CountOrders', regex: "\\/order\\/Orders\\/CountOrders", method: 'GET' },

            { path: '/order/Orders', regex: "\\/order\\/Orders$", method: 'GET' },
            { path: '/order/Orders/{id}', regex: "\\/order\\/Orders\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/order/Orders/{id}', regex: "\\/order\\/Orders\\/[a-zA-Z0-9]+", method: 'DELETE' },
            //#endregion order-api
            //#region delivery-api
            { path: '/delivery/Deliveries', regex: "\\/delivery\\/Deliveries$", method: 'GET' },
            { path: '/delivery/Deliveries/{id}', regex: "\\/delivery\\/Deliveries\\/[a-zA-Z0-9]+", method: 'PUT' },
            { path: '/delivery/Deliveries/{id}', regex: "\\/delivery\\/Deliveries\\/[a-zA-Z0-9]+", method: 'DELETE' },

            { path: '/delivery/Deliveries/Restaurant/{id}', regex: "\\/delivery\\/Deliveries\\/Restaurant\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/delivery/Deliveries/ChangeStatus', regex: "\\/delivery\\/Deliveries\\/ChangeStatus$", method: 'PUT' },

            { path: '/delivery/StatusesDeliveries', regex: "\\/delivery\\/StatusesDeliveries$", method: 'GET' },
            { path: '/delivery/StatusesDeliveries', regex: "\\/delivery\\/StatusesDeliveries$", method: 'POST' },
            { path: '/delivery/StatusesDeliveries/{id}', regex: "\\/delivery\\/StatusesDeliveries\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/delivery/StatusesDeliveries/{id}', regex: "\\/delivery\\/StatusesDeliveries\\/[a-zA-Z0-9]+", method: 'DELETE' }
            //#endregion delivery-api
        ],
        deliverer: [
            //#region auth-api
            { path: '/auth/security/logout', regex: "^\\/auth\\/security\\/logout$", method: 'DELETE' },
            { path: '/auth/security/token', regex: "^\\/auth\\/security\\/token$", method: 'POST' },
            { path: '/auth/security/me', regex: "^\\/auth\\/security\\/me$", method: 'GET' },
            //#endregion auth-api
            //#region order-api
            { path: '/order/Orders/{id}', regex: "\\/order\\/Orders\\/[a-zA-Z0-9]+", method: 'GET' },
            //#endregion order-api
            //#region delivery-api
            { path: '/delivery/Deliveries/Restaurant/{id}', regex: "\\/delivery\\/Deliveries\\/Restaurant\\/[a-zA-Z0-9]+", method: 'GET' },
            { path: '/delivery/Deliveries/ChangeStatus', regex: "\\/delivery\\/Deliveries\\/ChangeStatus$", method: 'PUT' },
            //#endregion delivery-api
        ],
        client: [
            //#region auth-api
            { path: '/auth/security/logout', regex: "^\\/auth\\/security\\/logout$", method: 'DELETE' },
            { path: '/auth/security/token', regex: "^\\/auth\\/security\\/token$", method: 'POST' },
            { path: '/auth/security/me', regex: "^\\/auth\\/security\\/me$", method: 'GET' },
            //#endregion auth-api
            //#region order-api
            { path: '/order/Orders/{id}', regex: "\\/order\\/Orders\\/[a-zA-Z0-9]+", method: 'GET' },
            //#endregion order-api
            //#region delivery-api
            { path: '/delivery/Deliveries/{id}', regex: "\\/delivery\\/Deliveries\\/[a-zA-Z0-9]+", method: 'PUT' },
            //#endregion delivery-api
        ],
    };

    static async seedPermissions(role, permissions) {
        // create permissions from persmissions array if not exists in db
        for (let i = 0; i < permissions.length; i++) {
            const permission = await Permission.findOne({ path: permissions[i].path, method: permissions[i].method }).exec();
            if (!permission) {
                await Permission.create({
                    path: permissions[i].path,
                    method: permissions[i].method,
                    regex: permissions[i].regex,
                    _role: role,
                });
            }
        }

        console.log('Permissions created.');
    }

    static async run() {
        try {
            const admin = await Role.findOne({ name: 'admin' }).exec();
            if (admin) {
                await this.seedPermissions(admin, this.permissionsByRole.admin);
                console.log('Admin permissions seeded and assigned.');
            } else {
                console.log('Admin role not found.');
            }

            const accountant = await Role.findOne({ name: 'accountant' }).exec();
            if (accountant) {
                await this.seedPermissions(accountant, this.permissionsByRole.accountant);
                console.log('Accountant permissions seeded and assigned.');
            } else {
                console.log('Accountant role not found.');
            }

            const deliverer = await Role.findOne({ name: 'deliverer' }).exec();
            if (deliverer) {
                await this.seedPermissions(deliverer, this.permissionsByRole.deliverer);
                console.log('Deliverer permissions seeded and assigned.');
            } else {
                console.log('Deliverer role not found.');
            }

            const client = await Role.findOne({ name: 'client' }).exec();
            if (client) {
                await this.seedPermissions(client, this.permissionsByRole.client);
                console.log('Client permissions seeded and assigned.');
            } else {
                console.log('Client role not found.');
            }

        } catch (e) {
            console.error('Error in permission seeding:', e);
        }
    }
}

module.exports = PermissionSeeder;