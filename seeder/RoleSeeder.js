const Role = require('../models/Role');

class RoleSeeder {

    static roles = [
        'admin',
        'accountant',
        'deliverer',
        'client',
    ];

    static async run() {
        try {
            for (const role of this.roles) {
                let exists = await Role.exists({ name: role }).exec();
                if (!exists) {
                    const newRole = new Role({
                        name: role,
                    });
    
                    await newRole.save(); // Wait for the save operation to complete
    
                    console.log(`${role} role created`);
                }
            }
            
            return;
        }
        catch (e) {
            console.log(e);
        }
    }
}

module.exports = RoleSeeder;