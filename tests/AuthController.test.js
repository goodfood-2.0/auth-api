const request = require('supertest');
const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const { MongoMemoryServer } = require('mongodb-memory-server');
const AuthController = require('../controllers/AuthController');
const User = require('../models/User');
const Role = require('../models/Role');
const { cryptPassword, comparePassword } = require('../utils/password');
const { randomKey } = require('../utils/random');

const { randomEmail } = require('../utils/random')

const JWT_ACCESS_KEY = "AZERTYUIOP123456789";
const JWT_REFRESH_KEY = "123456789AZERTYUIOP";

const app = express();
app.use(express.json());
app.post('/login', AuthController.login);
app.post('/register', AuthController.register);
app.post('/token', AuthController.token);
app.post('/logout', AuthController.logout);
app.post('/verify', AuthController.verify);
app.post('/reset', AuthController.reset);
app.post('/forgot', AuthController.forgot);
app.get('/me', AuthController.me);

describe('AuthController', () => {

  describe('login', () => {
    it('should return 400 if email or password is missing', async () => {
      const response = await request(app).post('/login').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: email, password');
    });

    it('should return 401 if user is not found', async () => {
      const response = await request(app).post('/login').send({ email: 'test@example.com', password: 'password' });
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('message', 'Unable to find user');
    });

    it('should return 401 if password is incorrect', async () => {
      const role = new Role({ name: 'client' });
      await role.save();

      const hashedPassword = await cryptPassword('correctpassword');
      const user = new User({
        email: 'test@example.com',
        password: hashedPassword.hash,
        _role: role._id,
      });
      await user.save();

      const response = await request(app).post('/login').send({ email: 'test@example.com', password: 'wrongpassword' });
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('message', 'Wrong credentials');
    });

    it('should return 200 and tokens if credentials are correct', async () => {
      const role = await Role.find({ name: "client" });

      const hashedPassword = await cryptPassword('correctpassword');
      const user = new User({
        email: 'test@example.com',
        password: hashedPassword.hash,
        _role: role._id,
      });
      await user.save();

      const response = await request(app).post('/login').send({ email: 'test@example.com', password: 'correctpassword' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('accessToken');
      expect(response.body).toHaveProperty('refreshToken');
      expect(response.body).toHaveProperty('user');
    });
  });

  describe('register', () => {
    it('should return 400 if required fields are missing', async () => {
      const response = await request(app).post('/register').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: email, firstname, lastname');
    });

    it('should return 400 if email is not valid', async () => {
      const response = await request(app).post('/register').send({ email: 'invalid-email', firstname: 'John', lastname: 'Doe' });
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Email is not valid');
    });

    it('should return 409 if email is already used', async () => {
      const role = await Role.find({ name: "client" });

      const user = new User({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: role._id,
      });
      await user.save();

      const response = await request(app).post('/register').send({ email: 'test@example.com', firstname: 'Jane', lastname: 'Doe' });
      expect(response.status).toBe(409);
      expect(response.body).toHaveProperty('message', 'Email already used');
    });

    it('should return 201 if registration is successful', async () => {
      const response = await request(app).post('/register').send({ email: 'test@example.com', firstname: 'John', lastname: 'Doe' });
      expect(response.status).toBe(201);
      expect(response.body).toHaveProperty('message', 'User registered successfully');
    });
  });

  describe('token', () => {
    it('should return 401 if refreshToken is missing', async () => {
      const response = await request(app).post('/token').send({});
      expect(response.status).toBe(401);
    });

    it('should return 403 if refreshToken is not found', async () => {
      const response = await request(app).post('/token').send({ refreshToken: 'invalidtoken' });
      expect(response.status).toBe(403);
    });

    it('should return 200 and a new accessToken if refreshToken is valid', async () => {
      const role = await Role.find({ name: "client" });

      const user = new User({
        email: 'test@example.com',
        refreshToken: jwt.sign({ _id: 'testuserid' }, process.env.JWT_REFRESH_KEY),
        _role: role._id,
      });
      await user.save();

      const response = await request(app).post('/token').send({ refreshToken: user.refreshToken });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('accessToken');
    });
  });

  describe('logout', () => {
    it('should return 403 if user is not found', async () => {
      const response = await request(app).post('/logout').set('Authorization', `Bearer invalidtoken`).send({});
      expect(response.status).toBe(403);
    });

    it('should return 200 if logout is successful', async () => {
      const role = await Role.find({ name: "client" });

      const user = new User({
        email: 'test@example.com',
        refreshToken: jwt.sign({ _id: 'testuserid' }, process.env.JWT_REFRESH_KEY),
        _role: role._id,
      });
      await user.save();

      const response = await request(app).post('/logout').set('Authorization', `Bearer ${user.refreshToken}`).send({});
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Logout successful');
    });
  });

  describe('verify', () => {
    it('should return 400 if required fields are missing', async () => {
      const response = await request(app).post('/verify').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: verificationToken, password, password_confirm');
    });

    it('should return 401 if user is not found', async () => {
      const response = await request(app).post('/verify').send({ verificationToken: 'invalidtoken', password: 'password', password_confirm: 'password' });
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('message', 'Unable to find user');
    });

    it('should return 200 if verification is successful', async () => {
      const role = await Role.find({ name: "client" });

      const user = new User({
        email: 'test@example.com',
        verificationToken: 'testtoken',
        _role: role._id,
      });
      await user.save();

      const response = await request(app).post('/verify').send({ verificationToken: 'testtoken', password: 'Password1!', password_confirm: 'Password1!' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Verification successful');
    });
  });

  describe('reset', () => {
    it('should return 400 if required fields are missing', async () => {
      const response = await request(app).post('/reset').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: verificationToken, password, password_confirm');
    });

    it('should return 401 if user is not found', async () => {
      const response = await request(app).post('/reset').send({ token: 'invalidtoken', password: 'password', password_confirm: 'password' });
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('message', 'Unable to find user');
    });

    it('should return 200 if password reset is successful', async () => {
      const role = await Role.find({ name: "client" });

      const user = new User({
        email: 'test@example.com',
        verificationToken: 'testtoken',
        _role: role._id,
      });
      await user.save();

      const response = await request(app).post('/reset').send({ token: 'testtoken', password: 'Password1!', password_confirm: 'Password1!' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Password reset successful');
    });
  });

  describe('forgot', () => {
    it('should return 400 if email is missing', async () => {
      const response = await request(app).post('/forgot').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: email');
    });

    it('should return 401 if user is not found', async () => {
      const response = await request(app).post('/forgot').send({ email: 'test@example.com' });
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('message', 'Unable to find user');
    });

    it('should return 200 if password reset email is sent', async () => {
      const role = await Role.find({ name: "client" });

      const user = new User({
        email: 'test@example.com',
        _role: role._id,
      });
      await user.save();

      const response = await request(app).post('/forgot').send({ email: 'test@example.com' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Password reset email sent');
    });
  });

  describe('me', () => {
    it('should return 403 if user is not authenticated', async () => {
      const response = await request(app).get('/me');
      expect(response.status).toBe(403);
    });

    it('should return 200 and user details if authenticated', async () => {
      const role = await Role.findOne({ name: "client" }).exec();

      const user = new User({
        email: randomEmail(),
        lastname: 'test',
        firstname: 'test',

        _role: role._id,
      });
      await user.save();

      const token = jwt.sign({ id: user._id }, JWT_ACCESS_KEY, { expiresIn: '1h' });

      const response = await request(app).get('/me').set('Authorization', `Bearer ${token}`);
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('email', 'test@example.com');
    });
  });
});
