const request = require('supertest');
const express = require('express');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const PermissionController = require('../controllers/PermissionController');
const Permission = require('../models/Permission');

const app = express();
app.use(express.json());
app.post('/permission/create', PermissionController.create);
app.get('/permission', PermissionController.get);
app.get('/permission/:id', PermissionController.getById);
app.put('/permission/:id', PermissionController.update);
app.delete('/permission', PermissionController.delete);

describe('PermissionController', () => {

  describe('create', () => {
    it('should return 400 if path is missing', async () => {
      const response = await request(app).post('/permission/create').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: path');
    });

    it('should return 200 and create permission if path is provided', async () => {
      const response = await request(app).post('/permission/create').send({ path: '/test/path' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Permission created');
      expect(response.body).toHaveProperty('permission');
    });
  });

  describe('get', () => {
    it('should return 200 and an array of permissions', async () => {
      await Permission.create({ path: '/test1' });
      await Permission.create({ path: '/test2' });

      const response = await request(app).get('/permission');
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Permissions');
      expect(response.body.permissions).toHaveLength(2);
    });
  });

  describe('getById', () => {
    it('should return 400 if id is missing', async () => {
      const response = await request(app).get('/permission/');
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: _id');
    });

    it('should return 404 if permission is not found', async () => {
      const response = await request(app).get('/permission/invalidid');
      expect(response.status).toBe(404);
      expect(response.body).toHaveProperty('message', 'Unable to find permission');
    });

    it('should return 200 and the permission if id is valid', async () => {
      const permission = await Permission.create({ path: '/test' });

      const response = await request(app).get(`/permission/${permission._id}`);
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Permission');
      expect(response.body).toHaveProperty('permission');
    });
  });

  describe('update', () => {
    it('should return 400 if path or id is missing', async () => {
      const response = await request(app).put('/permission/').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: path, _id');
    });

    it('should return 404 if permission is not found', async () => {
      const response = await request(app).put('/permission/invalidid').send({ path: '/newpath' });
      expect(response.status).toBe(404);
      expect(response.body).toHaveProperty('message', 'Unable to find permission');
    });

    it('should return 200 and update permission if id is valid', async () => {
      const permission = await Permission.create({ path: '/test' });

      const response = await request(app).put(`/permission/${permission._id}`).send({ path: '/newpath' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Permission updated');
      expect(response.body).toHaveProperty('permission');
      expect(response.body.permission.path).toBe('/newpath');
    });
  });

  describe('delete', () => {
    it('should return 400 if id is missing', async () => {
      const response = await request(app).delete('/permission').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: _id');
    });

    it('should return 404 if permission is not found', async () => {
      const response = await request(app).delete('/permission').send({ _id: 'invalidid' });
      expect(response.status).toBe(404);
      expect(response.body).toHaveProperty('message', 'Unable to find permission');
    });

    it('should return 200 and delete permission if id is valid', async () => {
      const permission = await Permission.create({ path: '/test' });

      const response = await request(app).delete('/permission').send({ _id: permission._id });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Permission deleted');
    });
  });

});
