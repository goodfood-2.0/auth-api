const request = require('supertest');
const express = require('express');
const UserController = require('../controllers/UserController');
const User = require('../models/User');

const app = express();
app.use(express.json());
app.post('/user/create', UserController.create);
app.get('/user', UserController.get);
app.get('/user/:id', UserController.getById);
app.put('/user/update', UserController.update);
app.delete('/user', UserController.delete);

describe('UserController', () => {

  describe('create', () => {
    it('should return 400 if any field is missing', async () => {
      const response = await request(app).post('/user').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing informations');
    });

    it('should return 200 and create user if all fields are provided', async () => {
      const response = await request(app).post('/user').send({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: "65a58d8395d3ff5ec83cc50e",
        _restaurant: 1,
      });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'User created');
      expect(response.body).toHaveProperty('user');
    });
  });

  describe('get', () => {
    it('should return 200 and an array of users', async () => {
      await User.create({
        email: 'test1@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: "65a58d8395d3ff5ec83cc50e",
        _restaurant: 1,
      });
      await User.create({
        email: 'test2@example.com',
        firstname: 'Jane',
        lastname: 'Smith',
        _role: "65a58d8395d3ff5ec83cc50a",
        _restaurant: 2,
      });

      const response = await request(app).get('/user');
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Users');
      expect(response.body.users).toHaveLength(2);
    });
  });

  describe('getById', () => {
    it('should return 404 if user is not found', async () => {
      const response = await request(app).get('/user/invalidid');
      expect(response.status).toBe(404);
      expect(response.body).toHaveProperty('message', 'User not found');
    });

    it('should return 200 and the user if id is valid', async () => {
      const user = await User.create({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: "65a58d8395d3ff5ec83cc50e",
        _restaurant: 1,
      });

      const response = await request(app).get(`/user/${user._id}`);
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'User');
      expect(response.body).toHaveProperty('user');
    });
  });

  describe('update', () => {
    it('should return 400 if no id provided', async () => {
      const response = await request(app).put(`/user/${user._id}`);
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'No id provided');
    });

    it('should return 404 if user not found', async () => {
      const response = await request(app).put(`/user/${user._id}`);
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'User not found');
    });

    it('should return 200 and update user if id is valid', async () => {
      const user = await User.create({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: "65a58d8395d3ff5ec83cc50e",
        _restaurant: 1,
      });

      const response = await request(app).put(`/user/${user._id}`).send({
        _id: user._id,
        email: 'updated@example.com',
        firstname: 'UpdatedJohn',
        lastname: 'UpdatedDoe',
        _role: "65a58d8395d3ff5ec83cc50a",
        _restaurant: 2,
      });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'User updated');
      expect(response.body).toHaveProperty('user');
    });
  });

  describe('delete', () => {
    it('should return 400 if no id provided', async () => {
      const response = await request(app).delete('/user/invalidid');
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'No id provided');
    });

    it('should return 400 if user not found', async () => {
      const response = await request(app).delete(`/user/${user._id}`);
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'User not found');
    });

    it('should return 200 and delete user if id is valid', async () => {
      const user = await User.create({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: "65a58d8395d3ff5ec83cc50e",
        _restaurant: 1,
      });

      const response = await request(app).delete(`/user/${user._id}`);
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'User deleted');
    });
  });

});
