const request = require('supertest');
const express = require('express');
const RoleController = require('../controllers/RoleController');
const Role = require('../models/Role');

const app = express();
app.use(express.json());
app.post('/role', RoleController.create);
app.get('/role', RoleController.get);
app.get('/role/:id', RoleController.getById);
app.put('/role/:id', RoleController.update);
app.delete('/role/:id', RoleController.delete);

describe('RoleController', () => {

  describe('create', () => {
    it('should return 400 if name is missing', async () => {
      const response = await request(app).post('/roles').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: name');
    });

    it('should return 200 and create role if name is provided', async () => {
      const response = await request(app).post('/roles').send({ name: 'testRole1' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Role created');
      expect(response.body).toHaveProperty('role');
    });
  });

  describe('get', () => {
    it('should return 200 and an array of roles', async () => {
      await Role.create({ name: 'testRole2' });
      await Role.create({ name: 'testRole3' });

      const response = await request(app).get('/roles');
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Roles');
      expect(response.body.roles).toHaveLength(2);
    });
  });

  describe('getById', () => {
    it('should return 400 if id is missing', async () => {
      const response = await request(app).get('/roles/');
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: _id');
    });

    it('should return 404 if role is not found', async () => {
      const response = await request(app).get('/roles/invalidid');
      expect(response.status).toBe(404);
      expect(response.body).toHaveProperty('message', 'Unable to find role');
    });

    it('should return 200 and the role if id is valid', async () => {
      const role = await Role.create({ name: 'testRole4' });

      const response = await request(app).get(`/roles/${role._id}`);
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Role');
      expect(response.body).toHaveProperty('role');
    });
  });

  describe('update', () => {
    it('should return 400 if name or id is missing', async () => {
      const response = await request(app).put('/roles/').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: name, _id');
    });

    it('should return 404 if role is not found', async () => {
      const response = await request(app).put('/roles/invalidid').send({ name: 'newName' });
      expect(response.status).toBe(404);
      expect(response.body).toHaveProperty('message', 'Unable to find role');
    });

    it('should return 200 and update role if id is valid', async () => {
      const role = await Role.create({ name: 'testRole5' });

      const response = await request(app).put(`/roles/${role._id}`).send({ name: 'newName' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Role updated');
      expect(response.body).toHaveProperty('role');
      expect(response.body.role.name).toBe('newName');
    });
  });

  describe('delete', () => {
    it('should return 400 if id is missing', async () => {
      const response = await request(app).delete('/roles').send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('message', 'Missing following informations: _id');
    });

    it('should return 404 if role is not found', async () => {
      const response = await request(app).delete('/roles').send({ _id: 'invalidid' });
      expect(response.status).toBe(404);
      expect(response.body).toHaveProperty('message', 'Role not found');
    });

    it('should return 200 and delete role if id is valid', async () => {
      const role = await Role.create({ name: 'testRole6' });

      const response = await request(app).delete(`/roles/${role._id}`).send();

      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('message', 'Role deleted');
    });
  });
});