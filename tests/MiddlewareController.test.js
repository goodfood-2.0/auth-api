const request = require('supertest');
const express = require('express');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const jwt = require('jsonwebtoken');
const MiddlewareController = require('../controllers/MiddlewareController');
const User = require('../models/User');
const Permission = require('../models/Permission');

const app = express();
app.use(express.json());
app.use((req, res, next) => {
  req.headers['x-auth-request-redirect'] = '/test';
  req.headers['x-original-method'] = 'GET';
  next();
});
app.use(MiddlewareController.middleware);
app.use(MiddlewareController.authenticate);

describe('MiddlewareController', () => {

  describe('middleware', () => {
    it('should return 200 if no permission needed', async () => {
      const response = await request(app).get('/test');
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('success', true);
      expect(response.body).toHaveProperty('message', 'No permission needed');
    });

    it('should return 403 if token not found', async () => {
      const response = await request(app).get('/test').set('Authorization', 'Bearer');
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('success', false);
      expect(response.body).toHaveProperty('message', 'Error, token not found');
    });

    it('should return 401 if token invalid', async () => {
      const response = await request(app).get('/test').set('Authorization', 'Bearer invalidtoken');
      expect(response.status).toBe(403);
      expect(response.body).toHaveProperty('success', false);
      expect(response.body).toHaveProperty('message', 'Error verification JWT');
    });

    it('should return 401 if user not found', async () => {
      const token = jwt.sign({ _id: 'invalidid' }, process.env.JWT_ACCESS_KEY);
      const response = await request(app).get('/test').set('Authorization', `Bearer ${token}`);
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('success', false);
      expect(response.body).toHaveProperty('message', 'Unauthorized route');
    });

    it('should return 401 if user role does not match permission role', async () => {
      const permission = await Permission.create({
        path: '/test',
        method: 'GET',
        _role: 'invalidrole',
      });
      const user = await User.create({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: 'validrole',
      });
      const token = jwt.sign({ _id: user._id }, process.env.JWT_ACCESS_KEY);
      const response = await request(app).get('/test').set('Authorization', `Bearer ${token}`);
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('success', false);
      expect(response.body).toHaveProperty('message', 'Unauthorized route');
    });

    it('should return 200 if user role matches permission role', async () => {
      const permission = await Permission.create({
        path: '/test',
        method: 'GET',
        _role: 'validrole',
      });
      const user = await User.create({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: 'validrole',
      });
      const token = jwt.sign({ _id: user._id }, process.env.JWT_ACCESS_KEY);
      const response = await request(app).get('/test').set('Authorization', `Bearer ${token}`);
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('success', true);
      expect(response.body).toHaveProperty('message', 'User authorized');
    });
  });

  describe('authenticate', () => {
    it('should pass middleware if no permission needed', async () => {
      const next = jest.fn();
      await MiddlewareController.authenticate({}, {}, next);
      expect(next).toHaveBeenCalled();
    });

    it('should pass middleware if permission not found', async () => {
      const next = jest.fn();
      await MiddlewareController.authenticate({
        method: 'GET',
        originalUrl: '/test',
      }, {}, next);
      expect(next).toHaveBeenCalled();
    });

    it('should pass middleware if token not provided', async () => {
      const next = jest.fn();
      await MiddlewareController.authenticate({
        method: 'GET',
        originalUrl: '/test',
      }, { status: () => ({ json: () => {} }) }, next);
      expect(next).toHaveBeenCalled();
    });

    it('should return 403 if token invalid', async () => {
      const next = jest.fn();
      const req = {
        method: 'GET',
        originalUrl: '/test',
        headers: {
          authorization: 'Bearer invalidtoken',
        },
      };
      const res = { status: jest.fn(() => ({ json: jest.fn() })) };
      await MiddlewareController.authenticate(req, res, next);
      expect(res.status).toHaveBeenCalledWith(403);
    });

    it('should return 401 if user not found', async () => {
      const next = jest.fn();
      const token = jwt.sign({ _id: 'invalidid' }, process.env.JWT_ACCESS_KEY);
      const req = {
        method: 'GET',
        originalUrl: '/test',
        headers: {
          authorization: `Bearer ${token}`,
        },
      };
      const res = { status: jest.fn(() => ({ json: jest.fn() })) };
      await MiddlewareController.authenticate(req, res, next);
      expect(res.status).toHaveBeenCalledWith(401);
    });

    it('should return 401 if user role does not match permission role', async () => {
      const next = jest.fn();
      const permission = await Permission.create({
        path: '/test',
        method: 'GET',
        _role: 'invalidrole',
      });
      const user = await User.create({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: 'validrole',
      });
      const token = jwt.sign({ _id: user._id }, process.env.JWT_ACCESS_KEY);
      const req = {
        method: 'GET',
        originalUrl: '/test',
        headers: {
          authorization: `Bearer ${token}`,
        },
      };
      const res = { status: jest.fn(() => ({ json: jest.fn() })) };
      await MiddlewareController.authenticate(req, res, next);
      expect(res.status).toHaveBeenCalledWith(401);
    });

    it('should pass middleware if user role matches permission role', async () => {
      const next = jest.fn();
      const permission = await Permission.create({
        path: '/test',
        method: 'GET',
        _role: 'validrole',
      });
      const user = await User.create({
        email: 'test@example.com',
        firstname: 'John',
        lastname: 'Doe',
        _role: 'validrole',
      });
      const token = jwt.sign({ _id: user._id }, process.env.JWT_ACCESS_KEY);
      const req = {
        method: 'GET',
        originalUrl: '/test',
        headers: {
          authorization: `Bearer ${token}`,
        },
      };
      const res = { status: jest.fn(() => ({ json: jest.fn() })) };
      await MiddlewareController.authenticate(req, res, next);
      expect(next).toHaveBeenCalled();
    });
  });

});
